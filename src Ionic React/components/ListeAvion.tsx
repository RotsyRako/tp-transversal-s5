import { IonButton, IonButtons, IonCardSubtitle, IonContent, IonFooter, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonSearchbar, IonText, IonToolbar } from '@ionic/react';

import { IonReactRouter } from '@ionic/react-router';
import { chevronBack, chevronForward, trashOutline } from 'ionicons/icons';
import ExploreContainer from './ExploreContainer';
//import styles from './Liste.module.scss';
import React,{useEffect,useState} from "react";
const ListeAvion: React.FC = () => {
	const loaddonnees=()=>{
        fetch("http://192.168.43.164:8080/avions",{method:"GET"})
        .then(response=>{
          response.text()
          .then(donnees=>{
         
            setPosts(JSON.parse(donnees))
            
          })
            
        }
        )
          
        }
        
    const [posts,setPosts]=useState<any[]>([
		loaddonnees()
	])
    
	class Voiture{
		idvehicule:number;

		constructor(idvehicule:number){
			this.idvehicule=idvehicule;	
		}
	}
    return (
      <IonPage >

			<IonHeader>
				<IonToolbar>
					<IonButtons >
						<IonButton >
							
							Liste Avion
						</IonButton>
					</IonButtons>

					<IonButtons slot="end">
						<IonButton routerLink="/movies" color="light">
							
							<IonIcon icon={ chevronForward } />
						</IonButton>
					</IonButtons>
				</IonToolbar>
			</IonHeader>
      <IonContent fullscreen>
				<div >

					

				<IonList>
						{ posts.map((post, index) => {
							var link="DetailsAvion/"+post?.idAvion;
							return (

								<IonItem  lines="none">
									
									<IonLabel>
                <h2>{post?.nomavion}</h2>
									</IonLabel>

									<IonButton href={link}>
										<IonText>Consulter</IonText>
									</IonButton>
								</IonItem>
							);
						})}
					</IonList>
				</div>
				<IonFooter>
									<IonButton href="/AssuranceAvion1/">
										<IonText>1 mois restant</IonText>
									</IonButton>
									<IonButton href="/AssuranceAvion3/">
										<IonText>3 mois restant</IonText>
									</IonButton>
				</IonFooter>
			</IonContent>
			
      </IonPage>
    );
  };

  export default ListeAvion;