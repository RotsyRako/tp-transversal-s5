import { IonButton, IonButtons, IonCardSubtitle, IonContent, IonFooter, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonSearchbar, IonText, IonToolbar } from '@ionic/react';

import { IonReactRouter } from '@ionic/react-router';
import { chevronBack, chevronForward, trashOutline } from 'ionicons/icons';
import ExploreContainer from './ExploreContainer';
//import styles from './Liste.module.scss';
import React,{useEffect,useState} from "react";
const ListeAvionAss3: React.FC = () => {
	const loaddonnees=()=>{
        fetch("http://192.168.43.164:8080/avionAssurance3",{method:"GET"})
        .then(response=>{
          response.text()
          .then(donnees=>{
         
            setPosts(JSON.parse(donnees))
            
          })
            
        }
        )
          
        }
        
    const [posts,setPosts]=useState<any[]>([
		loaddonnees()
	])
    
	class Voiture{
		idvehicule:number;

		constructor(idvehicule:number){
			this.idvehicule=idvehicule;	
		}
	}
    return (
      <IonPage >

			<IonHeader>
				<IonToolbar>
					<IonButtons >
						<IonButton >
							
							Liste Avion dont l'assurance expire dans 3 mois
						</IonButton>
					</IonButtons>

					<IonButtons slot="end">
						<IonButton routerLink="/movies" color="light">
							
							<IonIcon icon={ chevronForward } />
						</IonButton>
					</IonButtons>
				</IonToolbar>
			</IonHeader>
      <IonContent fullscreen>
				<div >

					

				<IonList>
						{ posts.map((post, index) => {
							var link="DetailsAvion/"+post?.idAvion;
							return (

								<IonItem  lines="none">
									
									<IonLabel>
                <h2>{post?.nomAvion}</h2>
									</IonLabel>

									<IonButton href={link}>
										<IonText>Consulter</IonText>
									</IonButton>
								</IonItem>
							);
						})}
					</IonList>
				</div>
				<IonButton>
					<IonText>SE CONNECTER</IonText>	
				</IonButton>
			</IonContent>
      </IonPage>
    );
  };

  export default ListeAvionAss3;