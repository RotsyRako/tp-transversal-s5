import { IonButton, IonButtons, IonCardSubtitle, IonContent, IonFooter, IonHeader, IonIcon, IonItem, IonLabel, IonList, IonPage, IonSearchbar, IonText, IonToolbar } from '@ionic/react';

import { IonReactRouter } from '@ionic/react-router';
import { chevronBack, chevronForward, trashOutline } from 'ionicons/icons';
import ExploreContainer from './ExploreContainer';
//import styles from './Liste.module.scss';
import React,{useEffect,useState} from "react";
import { useParams } from 'react-router';

const ListeAvion: React.FC<any> = () => {
    

    
    const idavion:any=useParams();
	const loaddonnees=()=>{
        console.log("http://192.168.43.164:8080/detailsAvions/"+idavion.idavion+"")
        fetch("http://192.168.43.164:8080/detailsAvions/"+idavion.idavion+"",{method:"GET"})
        .then(response=>{
          response.text()
          .then(donnees=>{
         
            setPosts(JSON.parse(donnees))
            console.log(JSON.parse(donnees))
          })
            
        }
        )
          
        }
    const [posts,setPosts]=useState<any>([
        loaddonnees()
    ])
    
    
    return (
      <IonPage >

			<IonHeader>
                
                <IonToolbar>
                        
                                
                                Details Avion
                </IonToolbar>
			</IonHeader>
            <IonContent fullscreen>
                    <IonList>
    
							

								<IonItem  lines="none">
									
									<IonLabel>
                <h2>Nom avion:{posts.nomavion}</h2>
									</IonLabel>
                                    <IonLabel>
                <h2>idavion:{posts.idAvion}</h2>
									</IonLabel>
                                    <IonLabel>
                <h2>Kilometrage:{posts.kilometres}</h2>
									</IonLabel>
                                    <IonLabel>
                <h2>Nombre de place:{posts.nbplaces}</h2>
									</IonLabel>
									<IonButton >
										<IonText>Consulter</IonText>
									</IonButton>
								</IonItem>
							
						
                        </IonList>
            </IonContent>
      </IonPage>
    );
  };

  export default ListeAvion;