import { Redirect, Route } from 'react-router-dom';
import { IonApp, IonRouterOutlet, setupIonicReact } from '@ionic/react';
import { IonReactRouter } from '@ionic/react-router';
import Home from './pages/Home';
import ListeVoiture from './components/ListeAvion';

/* Core CSS required for Ionic components to work properly */
import '@ionic/react/css/core.css';

/* Basic CSS for apps built with Ionic */
import '@ionic/react/css/normalize.css';
import '@ionic/react/css/structure.css';
import '@ionic/react/css/typography.css';

/* Optional CSS utils that can be commented out */
import '@ionic/react/css/padding.css';
import '@ionic/react/css/float-elements.css';
import '@ionic/react/css/text-alignment.css';
import '@ionic/react/css/text-transformation.css';
import '@ionic/react/css/flex-utils.css';
import '@ionic/react/css/display.css';

/* Theme variables */
import './theme/variables.css';
import ListeAvion from './components/ListeAvion';

import DetailsAvion from './components/DetailsAvion';
import ListeAvionAss3 from './components/ListeAvionAss3';
import ListeAvionAss1 from './components/ListeAvionAss1';
setupIonicReact();

const App: React.FC = () => (
  <IonApp>
    <IonReactRouter>
      <IonRouterOutlet>
        <Route exact path="/home">
          <ListeAvion />
        </Route>
        <Route exact path="/">
          <ListeAvion />
        </Route>
        <Route exact path="/DetailsAvion/:idavion">
          <DetailsAvion />
        </Route>
        <Route exact path="/AssuranceAvion1/">
          <ListeAvionAss1/>
        </Route>
        <Route exact path="/AssuranceAvion3/">
        <ListeAvionAss3/>
        </Route>
      </IonRouterOutlet>
    </IonReactRouter>
  </IonApp>
);

export default App;
